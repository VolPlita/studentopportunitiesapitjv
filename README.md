# Student Opportunities

Semester project for BIE-TJV

Students have plenty of opportunities to gain new experience or try new things, but it's hard to find them in the plethora of events, announcements and advertisements. The solution is portal for student leisure and study opportunities, which can help users filter and find opportunities of their interests, notify them about upcoming events and involve universities, companies and local communities.


## Running application:

 - Build the application using gradle with command (./gradlew build). It make take time as it is running all the tests.
 - Run the application with command (./gradlew bootRun)

 ### API documentation at:
 - http://localhost:8080/docs-api

 #### This is server application, there is client here: https://gitlab.com/VolPlita/studentopportunitiesclienttjv
