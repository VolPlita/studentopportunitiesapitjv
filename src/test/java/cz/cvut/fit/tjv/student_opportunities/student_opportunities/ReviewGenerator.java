package cz.cvut.fit.tjv.student_opportunities.student_opportunities;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Review;

public class ReviewGenerator implements EntityGenerator<Review>{
    @Override
    public Review createEntityWithoutId() {
        return new Review(5, "Good");
    }
}
