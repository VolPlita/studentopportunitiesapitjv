package cz.cvut.fit.tjv.student_opportunities.student_opportunities;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Opportunity;

import java.time.Duration;
import java.time.LocalDateTime;

public class OpportunityGenerator implements EntityGenerator<Opportunity>{
    @Override
    public Opportunity createEntityWithoutId() {
        return new Opportunity("Event", "For students", LocalDateTime.now(), LocalDateTime.now());
    }
}
