package cz.cvut.fit.tjv.student_opportunities.student_opportunities;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Role;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.User;

public class UserGenerator implements EntityGenerator<User>{
    @Override
    public User createEntityWithoutId() {
        return new User("Vova", Role.STUDENT);
    }
}
