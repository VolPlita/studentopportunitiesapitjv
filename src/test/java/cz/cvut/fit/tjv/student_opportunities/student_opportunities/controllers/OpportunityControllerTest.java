package cz.cvut.fit.tjv.student_opportunities.student_opportunities.controllers;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Opportunity;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Review;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Role;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.User;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.OpportunityRepository;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.ReviewRepository;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.UserRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@SpringBootTest
class OpportunityControllerIntegrationTest {

    @Autowired
    private OpportunityController opportunityController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OpportunityRepository opportunityRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @BeforeEach
    void setUp() {
        User testUser = new User("testuser1", "Userka Testova", Role.STUDENT);

        Opportunity op1 = new Opportunity("Op1", "op1", LocalDateTime.now(), LocalDateTime.now());
        op1.setEnrolledBy(List.of(testUser));
        Opportunity op2 = new Opportunity("Op2", "op2", LocalDateTime.now(), LocalDateTime.now());
        op2.setEnrolledBy(List.of(testUser));
        Opportunity op3 = new Opportunity("Op3", "op3", LocalDateTime.now(), LocalDateTime.now());
        op3.setEnrolledBy(List.of(testUser));
        Opportunity op4 = new Opportunity("Op4", "op4", LocalDateTime.now(), LocalDateTime.now());

        testUser.setEnrolledIn(List.of(op1, op2, op3));

        Review rev1 = new Review(2, "Not good");
        rev1.setReviewed(op1);
        rev1.setCreator(testUser);
        Review rev2 = new Review(4, "Very good");
        rev2.setReviewed(op2);
        rev2.setCreator(testUser);
        Review rev3 = new Review(5, "Excellent");
        rev3.setReviewed(op3);
        rev3.setCreator(testUser);

        op1.setReviews(List.of(rev1));
        op2.setReviews(List.of(rev2));
        op3.setReviews(List.of(rev3));

        userRepository.save(testUser);
        opportunityRepository.save(op1);
        opportunityRepository.save(op2);
        opportunityRepository.save(op3);
        opportunityRepository.save(op4);
        reviewRepository.save(rev1);
        reviewRepository.save(rev2);
        reviewRepository.save(rev3);
    }

    @AfterEach
    void clearUp() {
        reviewRepository.deleteAll();
        opportunityRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void shouldReturnAllFindAllOrEnrolledByUsernameOrRatedByUsername() {
        var ret = opportunityController
                .findAllOrEnrolledByUsernameOrRatedByUsername(
                        Optional.empty(),
                        Optional.empty());
        Iterable<Opportunity> result = ret.getBody();
        long len = StreamSupport.stream(result.spliterator(), false).count();
        Assertions.assertEquals(4, len);
    }

    @Test
    void shouldReturnThreeFindAllOrEnrolledByUsernameOrRatedByUsername() {
        var ret = opportunityController
                .findAllOrEnrolledByUsernameOrRatedByUsername(
                        Optional.of("testuser1"),
                        Optional.empty());
        Iterable<Opportunity> result = ret.getBody();
        Assertions.assertEquals(3,
                StreamSupport.stream(result.spliterator(), false).count());
    }

    @Test
    void shouldReturnTwoFindAllOrEnrolledByUsernameOrRatedByUsername() {
        var ret = opportunityController
                .findAllOrEnrolledByUsernameOrRatedByUsername(
                        Optional.of("testuser1"),
                        Optional.of(4));
        Iterable<Opportunity> result = ret.getBody();
        Assertions.assertEquals(2,
                StreamSupport.stream(result.spliterator(), false).count());
    }
}