package cz.cvut.fit.tjv.student_opportunities.student_opportunities;

public interface EntityGenerator<T> {
    public T createEntityWithoutId();
}
