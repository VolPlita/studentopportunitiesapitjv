package cz.cvut.fit.tjv.student_opportunities.student_opportunities.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Role;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.User;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions.EntityCannotBeCreatedException;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.services.UserService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class UserControllerApiTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService service;

    @Test
    void createSuccess() throws Exception {
        User testUser1 = new User("testuser", "User Testovich", Role.STUDENT);
        Mockito.when(service.create(Mockito.any(User.class))).thenReturn(testUser1);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/user")
                .content(new ObjectMapper().writeValueAsString(testUser1))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username", Matchers.is(testUser1.getUsername())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.realName", Matchers.is(testUser1.getRealName())));
        Mockito.verify(service).create(Mockito.any(User.class));
    }

    @Test
    void createFail() throws Exception {
        User testUser1 = new User("testuser", "User Testovich", Role.STUDENT);
        Mockito.when(service.create(Mockito.any(User.class))).thenThrow(EntityCannotBeCreatedException.class);
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/user")
                        .content(new ObjectMapper().writeValueAsString(testUser1))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }
}