package cz.cvut.fit.tjv.student_opportunities.student_opportunities.services;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.ReviewGenerator;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.UserGenerator;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Opportunity;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Review;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.User;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions.EntityDoesNotExistException;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions.EntityNotInListException;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions.TimeException;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.OpportunityRepository;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.ReviewRepository;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ReviewServiceUnitTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private OpportunityRepository opportunityRepository;
    @Mock
    private ReviewRepository reviewRepository;
    @InjectMocks
    private ReviewService service;
    /*
    1. When the user does not exist.
    2. When the opportunity does not exist.
    3. When the user is not enrolled in the opportunity.
    4. When the opportunity is not yet finished.
    5. When a review is successfully created.
    */
    @Test
    void createReviewUserDoesNotExistThrowsException() {
        String creatorUsername = "nonexistentUser";
        Long opportunityId = 1L;
        Review review = new Review();

        Mockito.when(userRepository.findById(creatorUsername)).thenReturn(Optional.empty());

        assertThrows(EntityDoesNotExistException.class,
                () -> service.createReview(review, creatorUsername, opportunityId));
    }

    @Test
    void createReviewOpportunityDoesNotExistThrowsException() {
        String creatorUsername = "existingUser";
        Long opportunityId = 1L;
        Review review = new Review();

        Mockito.when(userRepository.findById(creatorUsername)).thenReturn(Optional.of(new User()));
        Mockito.when(opportunityRepository.findById(opportunityId)).thenReturn(Optional.empty());

        assertThrows(EntityDoesNotExistException.class,
                () -> service.createReview(review, creatorUsername, opportunityId));
    }

    @Test
    void createReviewUserNotEnrolledThrowsException() {
        UserGenerator userGenerator = new UserGenerator();
        ReviewGenerator reviewGenerator = new ReviewGenerator();

        String creatorUsername = "notEnrolledUser";
        Long opportunityId = 1L;

        Opportunity opportunity = new Opportunity();
        User student = userGenerator.createEntityWithoutId();
        Review review = reviewGenerator.createEntityWithoutId();

        Mockito.when(userRepository.findById(creatorUsername)).thenReturn(Optional.of(student));
        Mockito.when(opportunityRepository.findById(opportunityId)).thenReturn(Optional.of(opportunity));
        opportunity.setEnrolledBy(List.of(userGenerator.createEntityWithoutId()));

        assertThrows(EntityNotInListException.class,
                () -> service.createReview(review, creatorUsername, opportunityId));
    }

    @Test
    void createReviewOpportunityNotFinishedThrowsException() {
        UserGenerator userGenerator = new UserGenerator();
        ReviewGenerator reviewGenerator = new ReviewGenerator();

        String creatorUsername = "testUser";
        Long opportunityId = 1L;

        Opportunity opportunity = new Opportunity();
        User student = userGenerator.createEntityWithoutId();
        Review review = reviewGenerator.createEntityWithoutId();

        Collection<User> insertedUsers = Set.of(student);
        opportunity.setEnrolledBy(insertedUsers);
//        opportunity.setStart(LocalDateTime.MAX);
        opportunity.setFinish(LocalDateTime.MAX);

        Mockito.when(userRepository.findById(creatorUsername)).thenReturn(Optional.of(student));
        Mockito.when(opportunityRepository.findById(opportunityId)).thenReturn(Optional.of(opportunity));
//        Mockito.when(opportunity.getEnrolledBy().contains(student)).thenReturn(true);
//        Mockito.when(opportunity.getFinish().isAfter(LocalDateTime.now()))
//                .thenReturn(true);

        assertThrows(TimeException.class,
                () -> service.createReview(review, creatorUsername, opportunityId));
    }

    @Test
    void createReviewSuccess() {
        UserGenerator userGenerator = new UserGenerator();
        ReviewGenerator reviewGenerator = new ReviewGenerator();

        String creatorUsername = "testUser";
        Long opportunityId = 1L;

        Opportunity opportunity = new Opportunity();
        User student = userGenerator.createEntityWithoutId();
        Review review = reviewGenerator.createEntityWithoutId();

        Collection<User> insertedUsers = Set.of(student);
        opportunity.setEnrolledBy(insertedUsers);

        Mockito.when(userRepository.findById(creatorUsername)).thenReturn(Optional.of(student));
        Mockito.when(opportunityRepository.findById(opportunityId)).thenReturn(Optional.of(opportunity));
        Mockito.when(reviewRepository.save(review)).thenReturn(review);

        var ret = service.createReview(review, creatorUsername, opportunityId);
        assertEquals(review, ret);
        Mockito.verify(service.repository, Mockito.atLeastOnce()).save(review);
    }
}