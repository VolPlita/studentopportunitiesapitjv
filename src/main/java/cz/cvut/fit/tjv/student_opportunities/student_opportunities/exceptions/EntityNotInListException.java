package cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions;

public class EntityNotInListException extends RuntimeException{
    public EntityNotInListException(String message) {
        super(message);
    }
}
