package cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions;

public class TimeException extends RuntimeException {
    public TimeException(String message) {
        super(message);
    }
}
