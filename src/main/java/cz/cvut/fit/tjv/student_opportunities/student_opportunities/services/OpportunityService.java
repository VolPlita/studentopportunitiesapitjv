package cz.cvut.fit.tjv.student_opportunities.student_opportunities.services;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Opportunity;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Role;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.User;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions.*;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.OpportunityRepository;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.ReviewRepository;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

@Service
public class OpportunityService extends CrudService<Opportunity, Long, OpportunityRepository> {

    private final UserRepository userRepository;

    private final ReviewRepository reviewRepository;

    public OpportunityService(OpportunityRepository repository, UserRepository userRepository, ReviewRepository reviewRepository) {
        super(repository);
        this.userRepository = userRepository;
        this.reviewRepository = reviewRepository;
    }

    public Opportunity createOpportunity(Opportunity opportunity, String creatorUsername) {
        User creator = userRepository
                .findById(creatorUsername)
                .orElseThrow(() -> new EntityDoesNotExistException("Username does not exist"));
        if (creator.getRole() != Role.ORGANIZER)
            throw new WrongRoleException("User is not organiser");
        if (opportunity.getFinish().isBefore(opportunity.getStart()))
            throw new EntityCannotBeCreatedException("Start is later than finish.");
        opportunity.setCreator(creator); // Not needed?
        return create(opportunity);
    }

    public void updateOpportunity(Long id, Opportunity opportunity, String creatorUsername) {
        User creator = userRepository
                .findById(creatorUsername)
                .orElseThrow(() -> new EntityDoesNotExistException("Username does not exist"));
        Optional<Opportunity> updatedOpportunity = repository.findById(id);
        if (updatedOpportunity.isPresent() && !updatedOpportunity.get().getCreator().equals(creator))
            throw new WrongRoleException("User is not creator of this opportunity");
        opportunity.setCreator(creator);
        update(id, opportunity);
    }

    public void deleteOpportunity(Long id, String creatorUsername) {
        User creator = userRepository
                .findById(creatorUsername)
                .orElseThrow(() -> new EntityDoesNotExistException("Username does not exist"));
        Optional<Opportunity> toBeDeletedOpportunity = repository.findById(id);
        if (toBeDeletedOpportunity.isPresent() && !toBeDeletedOpportunity.get().getCreator().equals(creator))
            throw new WrongRoleException("User is not creator of this opportunity");
        toBeDeletedOpportunity.ifPresent(opportunity -> reviewRepository.deleteAll(opportunity.getReviews()));
        deleteById(id);
    }

    public void enrollStudentInOpportunity(Long opportunityId, String username) {
        Optional<User> optionalUser = userRepository.findById(username);
        if (optionalUser.isEmpty()) throw new EntityDoesNotExistException("No student with such username");
        Optional<Opportunity> optionalOpportunity = repository.findById(opportunityId);
        if (optionalOpportunity.isEmpty()) throw new EntityDoesNotExistException("No opportunity with such id");

        User student = optionalUser.get();
        Opportunity opportunity = optionalOpportunity.get();

        if (opportunity.getCreator().equals(student))
            throw new WrongRoleException("Creator can't enroll in their opportunity");
        if (opportunity.getEnrolledBy().contains(student))
            throw new EntityAlreadyInListException("Student already enrolled in this opportunity");
        if (opportunity.getStart().isAfter(LocalDateTime.now()))
            throw new TimeException("Opportunity already started");
        // Add check for finished opportunity

//        student.getEnrolledIn().add(opportunity);
        Collection<User> mutedEnrolledBy = opportunity.getEnrolledBy();
        mutedEnrolledBy.add(student);
        opportunity.setEnrolledBy(mutedEnrolledBy);

        userRepository.save(student);
        update(opportunityId, opportunity);
    }

    public void removeStudentFromOpportunity(Long opportunityId, String username) {
        Optional<User> optionalUser = userRepository.findById(username);
        if (optionalUser.isEmpty()) throw new EntityDoesNotExistException("No student with such username");
        Optional<Opportunity> optionalOpportunity = repository.findById(opportunityId);
        if (optionalOpportunity.isEmpty()) throw new EntityDoesNotExistException("No opportunity with such id");

        User student = optionalUser.get();
        Opportunity opportunity = optionalOpportunity.get();

        if (!opportunity.getEnrolledBy().contains(student))
            throw new EntityNotInListException("Student wasn't enrolled in opportunity");
        if (opportunity.getStart().isBefore(LocalDateTime.now()))
            throw new TimeException("Opportunity already started");

//        opportunity.getEnrolledBy().remove(student);
        Collection<User> mutedEnrolledBy = opportunity.getEnrolledBy();
        mutedEnrolledBy.remove(student);
        opportunity.setEnrolledBy(mutedEnrolledBy);
//        student.getEnrolledIn().remove(opportunity);

        userRepository.save(student);
        update(opportunityId, opportunity);
    }

    public Collection<Opportunity> findAllRatedByUser(String username, int rating) {
        return repository.findAllRatedByUsername(username, rating);
    }

    public Collection<Opportunity> findAllEnrolledByUser(String enrolledUsername) {
        return repository.findAllEnrolledByUsername(enrolledUsername);
    }
}
