package cz.cvut.fit.tjv.student_opportunities.student_opportunities.controllers;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Opportunity;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.OpportunityRepository;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.services.OpportunityService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/opportunity")
public class OpportunityController extends CrudController<Opportunity, Long, OpportunityService, OpportunityRepository>{
    public OpportunityController(OpportunityService service) {
        super(service);
    }

    @PostMapping
    public ResponseEntity<Opportunity> createOpportunity(@RequestBody Opportunity opportunity, @RequestParam("username") String username) {
        Opportunity createdOpportunity = service.createOpportunity(opportunity, username);
        return ResponseEntity.ok(createdOpportunity);
    }

    @GetMapping
    public ResponseEntity<Iterable<Opportunity>> findAllOrEnrolledByUsernameOrRatedByUsername(
            @RequestParam("username") Optional<String> username,
            @RequestParam("rating") Optional<Integer> rating) {
        if (username.isEmpty() && rating.isEmpty()) {
            return ResponseEntity.ok(service.readAll());
        }
        if (rating.isEmpty()) {
            return ResponseEntity.ok(service.findAllEnrolledByUser(username.get()));
        }
        Collection<Opportunity> opportunities = service.findAllRatedByUser(username.get(), rating.get());
        return ResponseEntity.ok(opportunities);
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Opportunity readById(@PathVariable Long id) {
        var optE = service.readById(id);
        if (optE.isPresent()) return optE.get();
        else throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    public void updateOpportunity(@PathVariable Long id, @RequestBody Opportunity opportunity, @RequestParam("username") String username) {
        service.updateOpportunity(id, opportunity, username);
    }

    @DeleteMapping("/{id}")
    public void deleteOpportunity(@PathVariable Long id, @RequestParam("username") String username) {
        service.deleteOpportunity(id, username);
    }

    @PutMapping("/{id}/enroll") //
    public void enrollStudentInOpportunity(@PathVariable Long id, @RequestParam("username") String username) {
        service.enrollStudentInOpportunity(id, username);
    }

    @PutMapping("/{id}/remove") //
    public void removeStudentFromOpportunity(@PathVariable Long id, @RequestParam("username") String username) {
        service.removeStudentFromOpportunity(id, username);
    }
}
