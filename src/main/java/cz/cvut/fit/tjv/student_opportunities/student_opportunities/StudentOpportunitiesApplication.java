package cz.cvut.fit.tjv.student_opportunities.student_opportunities;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentOpportunitiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentOpportunitiesApplication.class, args);
	}

}
