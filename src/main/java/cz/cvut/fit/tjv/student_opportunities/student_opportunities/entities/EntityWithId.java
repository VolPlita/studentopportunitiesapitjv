package cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities;

public interface EntityWithId<ID> {
    ID getId();
    void setId(ID id);
}
