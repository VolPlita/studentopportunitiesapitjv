package cz.cvut.fit.tjv.student_opportunities.student_opportunities.controllers;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.User;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.UserRepository;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/user")
public class UserController extends CrudController<User, String, UserService, UserRepository>{
    public UserController(UserService service) {
        super(service);
    }

    @PostMapping
    @ResponseBody
    public User create(@RequestBody User data) {
        var res = service.create(data);
        return res;
    }

    @GetMapping
    @ResponseBody
    public Iterable<User> readAll() {
        return service.readAll();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public User readById(@PathVariable String id) {
        var optE = service.readById(id);
        if (optE.isPresent()) return optE.get();
        else throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT) // ?
    public void update(@PathVariable String id, @RequestBody User data) {
        service.update(id, data);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT) // ?
    public void deleteById(@PathVariable String id) {
        service.deleteById(id);
    }
}
