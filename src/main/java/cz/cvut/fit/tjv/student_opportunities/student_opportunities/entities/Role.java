package cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities;

public enum Role {
    STUDENT,
    ORGANIZER
}
