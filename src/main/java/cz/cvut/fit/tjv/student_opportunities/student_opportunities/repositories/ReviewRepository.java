package cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Review;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ReviewRepository extends CrudRepository<Review, Long> {
    @Query("SELECT o.reviews FROM Opportunity o WHERE o.id = :id")
    Collection<Review> findAllReviewsOfOpportunity(Long id);
}
