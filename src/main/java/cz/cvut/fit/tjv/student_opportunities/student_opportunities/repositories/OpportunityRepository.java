package cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Opportunity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface OpportunityRepository extends CrudRepository<Opportunity, Long> {
    /*
    Complex query:
    Get all opportunities that user X rated at least Y/5.
     */
    @Query("SELECT r.reviewed FROM Review r WHERE r.creator.username = :username AND r.rating >= :rating")
    Collection<Opportunity> findAllRatedByUsername(String username, int rating);

    /*
    Get opportunities enrolled by user X
     */
    @Query("SELECT u.enrolledIn FROM User u WHERE u.username = :username")
    Collection<Opportunity> findAllEnrolledByUsername(String username);
}
