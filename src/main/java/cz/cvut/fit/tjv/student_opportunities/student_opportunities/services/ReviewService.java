package cz.cvut.fit.tjv.student_opportunities.student_opportunities.services;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Opportunity;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Review;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.User;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions.EntityDoesNotExistException;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions.EntityNotInListException;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions.TimeException;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions.WrongRoleException;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.OpportunityRepository;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.ReviewRepository;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

@Service
public class ReviewService extends CrudService<Review, Long, ReviewRepository> {

    private final UserRepository userRepository;
    private final OpportunityRepository opportunityRepository;
    public ReviewService(ReviewRepository repository, UserRepository userRepository, OpportunityRepository opportunityRepository) {
        super(repository);
        this.userRepository = userRepository;
        this.opportunityRepository = opportunityRepository;
    }

    public Review createReview(Review review, String creator, Long opportunityId) {
        Optional<User> optionalUser = userRepository.findById(creator);
        if (optionalUser.isEmpty()) throw new EntityDoesNotExistException("No such user");
        Optional<Opportunity> optionalOpportunity = opportunityRepository.findById(opportunityId);
        if (optionalOpportunity.isEmpty()) throw new EntityDoesNotExistException("No such opportunity");

        User student = optionalUser.get();
        Opportunity opportunity = optionalOpportunity.get();

        if (!opportunity.getEnrolledBy().contains(student))
            throw new EntityNotInListException("Student didn't enroll this opportunity");
        if (opportunity.getFinish().isAfter(LocalDateTime.now()))
            throw new TimeException("Opportunity is not finished yet");

        review.setReviewed(opportunity);
        review.setCreator(student);
//        student.getReviews().add(review);
//        userRepository.save(student);
//        opportunity.getReviews().add(review);
//        opportunityRepository.save(opportunity);
        return create(review);
    }

    public void editReview(Long id, Review review, String creatorUsername) {
        User creator = userRepository
                .findById(creatorUsername)
                .orElseThrow(() -> new EntityDoesNotExistException("Username does not exist"));
        Review editedReview = repository
                .findById(id)
                .orElseThrow(() -> new EntityDoesNotExistException("No review with such id"));
        if (!editedReview.getCreator().equals(creator))
            throw new WrongRoleException("Student didn't create this review");
        update(id, review);
    }

    public void deleteReview(Long id, String creatorUsername) {
        User creator = userRepository
                .findById(creatorUsername)
                .orElseThrow(() -> new EntityDoesNotExistException("Username does not exist"));
        Review editedReview = repository
                .findById(id)
                .orElseThrow(() -> new EntityDoesNotExistException("No review with such id"));
        if (!editedReview.getCreator().equals(creator))
            throw new WrongRoleException("Student didn't create this review");
        deleteById(id);
    }

    public Collection<Review> findAllReviewsOfOpportunity(Long id) {
        return repository.findAllReviewsOfOpportunity(id);
    }
}
