package cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Collection;
import java.util.Collections;

@Entity
@Table(name = "users")
public class User implements EntityWithId<String>{
    @Id
    private String username;
    private String realName;
    private Role role;
    @ManyToMany(mappedBy = "enrolledBy", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Collection<Opportunity> enrolledIn;
    @OneToMany(mappedBy = "creator", cascade = CascadeType.ALL)
    private Collection<Opportunity> created;
    @OneToMany(mappedBy = "creator", cascade = CascadeType.ALL)
    @JsonIgnore
    private Collection<Review> reviews;

    public User() {}

    public User(String realName, Role role) {
        this.realName = realName;
        this.role = role;
    }

    public User(String username, String realName, Role role) {
        this.username = username;
        this.realName = realName;
        this.role = role;
    }

    @Override
    public String getId() {
        return username;
    }

    @Override
    public void setId(String username) {
        this.username = username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setEnrolledIn(Collection<Opportunity> enrolledIn) {
        this.enrolledIn = enrolledIn;
    }

    public void setCreated(Collection<Opportunity> created) {
        this.created = created;
    }

    public void setReviews(Collection<Review> reviews) {
        this.reviews = reviews;
    }

    public Role getRole() {
        return role;
    }

    public String getUsername() {
        return username;
    }

    public String getRealName() {
        return realName;
    }

    public Collection<Opportunity> getEnrolledIn() {
        return enrolledIn;
    }

    public Collection<Opportunity> getCreated() {
        return created;
    }

    public Collection<Review> getReviews() {
        return reviews;
    }
}
