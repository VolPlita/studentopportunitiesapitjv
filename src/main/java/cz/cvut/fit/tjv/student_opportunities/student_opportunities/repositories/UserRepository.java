package cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
//    Collection<User> findAllByRealName(String realName);
}
