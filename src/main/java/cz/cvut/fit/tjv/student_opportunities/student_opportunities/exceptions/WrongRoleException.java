package cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions;

public class WrongRoleException extends RuntimeException {
    public WrongRoleException(String message) {
        super(message);
    }
}
