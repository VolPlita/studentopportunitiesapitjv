package cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(value = {EntityCannotBeCreatedException.class})
    public ResponseEntity<Object> handleEntityCannotBeCreatedException(EntityCannotBeCreatedException e) {
        HttpStatus badRequest = HttpStatus.CONFLICT;
        // 1) Create payload with exception details
        ApiException apiException = new ApiException(e.getMessage(), badRequest, ZonedDateTime.now());
        // 2) Return response entity
        return new ResponseEntity<>(apiException, badRequest);
    }

    @ExceptionHandler(value = {EntityDoesNotExistException.class})
    public ResponseEntity<Object> handleEntityDoesNotExistException(EntityDoesNotExistException e) {
        HttpStatus badRequest = HttpStatus.NOT_FOUND;
        ApiException apiException = new ApiException(e.getMessage(), badRequest, ZonedDateTime.now());
        return new ResponseEntity<>(apiException, badRequest);
    }

    @ExceptionHandler(value = {WrongRoleException.class})
    public  ResponseEntity<Object> handleWrongRoleException(WrongRoleException e) {
        HttpStatus badRequest = HttpStatus.FORBIDDEN;
        ApiException apiException = new ApiException(e.getMessage(), badRequest, ZonedDateTime.now());
        return new ResponseEntity<>(apiException, badRequest);
    }

    @ExceptionHandler(value = {EntityAlreadyInListException.class})
    public ResponseEntity<Object> handleEntityAlreadyInListException(EntityAlreadyInListException e) {
        HttpStatus badRequest = HttpStatus.CONFLICT;
        ApiException apiException = new ApiException(e.getMessage(), badRequest, ZonedDateTime.now());
        return new ResponseEntity<>(apiException, badRequest);
    }

    @ExceptionHandler(value = {TimeException.class})
    public ResponseEntity<Object> handleTimeException(TimeException e) {
        HttpStatus badRequest = HttpStatus.FORBIDDEN;
        ApiException apiException = new ApiException(e.getMessage(), badRequest, ZonedDateTime.now());
        return new ResponseEntity<>(apiException, badRequest);
    }

    @ExceptionHandler(value = {EntityNotInListException.class})
    public ResponseEntity<Object> handleEntityNotInListException(EntityNotInListException e) {
        HttpStatus badRequest = HttpStatus.FORBIDDEN;
        ApiException apiException = new ApiException(e.getMessage(), badRequest, ZonedDateTime.now());
        return new ResponseEntity<>(apiException, badRequest);
    }
}
