package cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions;

import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

public record ApiException(String message, HttpStatus httpStatus, ZonedDateTime timeStamp) {
}

//public class ApiException {
//    private final String message;
//    private final HttpStatus httpStatus;
//    private final ZonedDateTime timeStamp;
//
//    public ApiException(String message, HttpStatus httpStatus, ZonedDateTime timeStamp) {
//        this.message = message;
//        this.httpStatus = httpStatus;
//        this.timeStamp = timeStamp;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public HttpStatus getHttpStatus() {
//        return httpStatus;
//    }
//
//    public ZonedDateTime getTimeStamp() {
//        return timeStamp;
//    }
//}
