package cz.cvut.fit.tjv.student_opportunities.student_opportunities.services;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.EntityWithId;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions.EntityCannotBeCreatedException;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions.EntityDoesNotExistException;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

public abstract class CrudService<E extends EntityWithId<ID>, ID, R extends CrudRepository<E, ID>> { // Use data layer
    protected R repository;

    public CrudService(R repository) {
        this.repository = repository;
    }

    public E create(E data) {
        // Make some method to get id's of entities
        // Where to put this method?
        // Look at entityWithId
//        if (repository.existsById(data.getId()))
//            throw new EntityCannotBeCreatedException("No entity to create");
        return repository.save(data);
    }

    public Iterable<E> readAll() {
        return repository.findAll();
    }

    public Optional<E> readById(ID id) {
        return repository.findById(id);
    }

    public void update(ID id, E data) {
        if (!repository.existsById(id))
            throw new EntityDoesNotExistException("No entity to update");
        data.setId(id);
        repository.save(data);
    }

    public void deleteById(ID id) {
        if (!repository.existsById(id))
            throw new EntityDoesNotExistException("No entity to delete");
        repository.deleteById(id);
    }

}
