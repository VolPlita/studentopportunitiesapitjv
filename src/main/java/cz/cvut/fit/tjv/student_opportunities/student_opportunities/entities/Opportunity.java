package cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
public class Opportunity implements EntityWithId<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private LocalDateTime start;
    private LocalDateTime finish;
    @ManyToMany(cascade = CascadeType.REMOVE)
    @JoinTable (name = "enrollment",
            joinColumns = @JoinColumn(name = "opportunity_id"),
            inverseJoinColumns = @JoinColumn(name = "user_username")
    )
    @JsonIgnore
    private Collection<User> enrolledBy;

    @ManyToOne
    @JoinColumn(name = "creator_username")
    @JsonIgnore
    private User creator;
    @OneToMany(mappedBy = "reviewed", cascade = CascadeType.ALL)
    private Collection<Review> reviews;

    public Opportunity() {
        title = "No title";
        description = "No description";
        start = LocalDateTime.now();
        finish = LocalDateTime.now();
    }

    public Opportunity(String title, String description, LocalDateTime start, LocalDateTime finish) {
        this.title = title;
        this.description = description;
        this.start = start;
        this.finish = finish;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public void setFinish(LocalDateTime finish) {
        this.finish = finish;
    }

    public void setEnrolledBy(Collection<User> enrolledBy) {
        this.enrolledBy = enrolledBy;
    }

    public void setReviews(Collection<Review> reviews) {
        this.reviews = reviews;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getFinish() {
        return finish;
    }

    public Collection<User> getEnrolledBy() {
        return enrolledBy;
    }

    public User getCreator() {
        return creator;
    }

    public Collection<Review> getReviews() {
        return reviews;
    }
}
