package cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions;

public class EntityAlreadyInListException extends RuntimeException{
    public EntityAlreadyInListException(String message) {
        super(message);
    }
}
