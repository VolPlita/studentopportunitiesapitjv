package cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

@Entity
public class Review implements EntityWithId<Long>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int rating;
    private String comment;
    @ManyToOne
    @JoinColumn(name = "creator_username")
    private User creator;
    @ManyToOne
    @JoinColumn(name = "opportunity_id")
    @JsonIgnore
    private Opportunity reviewed;

    public Review() {}

    public Review(int rating, String comment) {
        this.rating = rating;
        this.comment = comment;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public void setReviewed(Opportunity reviewed) {
        this.reviewed = reviewed;
    }

    public int getRating() {
        return rating;
    }

    public String getComment() {
        return comment;
    }

    public User getCreator() {
        return creator;
    }

    public Opportunity getReviewed() {
        return reviewed;
    }
}
