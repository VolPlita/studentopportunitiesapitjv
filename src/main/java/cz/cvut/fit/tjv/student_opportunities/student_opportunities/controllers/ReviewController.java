package cz.cvut.fit.tjv.student_opportunities.student_opportunities.controllers;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Review;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.ReviewRepository;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.services.ReviewService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/review")
public class ReviewController extends CrudController<Review, Long, ReviewService, ReviewRepository>{
    public ReviewController(ReviewService service) {
        super(service);
    }

    @PostMapping
    public ResponseEntity<Review> createReview(@RequestBody Review review, @RequestParam("username") String username, @RequestParam("id") Long id) {
        Review createdReview = service.createReview(review, username, id);
        return ResponseEntity.ok(createdReview);
    }

    @GetMapping
    @ResponseBody
    public Iterable<Review> readAll() {
        return service.readAll();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Review readById(@PathVariable Long id) {
        var optE = service.readById(id);
        if (optE.isPresent()) return optE.get();
        else throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    public void editReview(@PathVariable Long id, @RequestBody Review review, @RequestParam("username") String username) {
        service.editReview(id, review, username);
    }

    @DeleteMapping("/{id}")
    public void deleteReview(@PathVariable Long id, @RequestParam(name = "username") String username) {
        service.deleteReview(id, username);
    }

    @GetMapping("/of")
    public Iterable<Review> readAllReviewsOfOpportunity(@RequestParam(value = "opportunityId") Long opportunityId) {
        return service.findAllReviewsOfOpportunity(opportunityId);
    }
}
