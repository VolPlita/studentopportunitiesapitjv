package cz.cvut.fit.tjv.student_opportunities.student_opportunities.services;

import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.Opportunity;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.entities.User;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions.*;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.OpportunityRepository;
import cz.cvut.fit.tjv.student_opportunities.student_opportunities.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;


@Service
public class UserService extends CrudService<User, String, UserRepository> {

    public UserService(UserRepository repository, OpportunityRepository opportunityRepository) {
        super(repository);
    }

//    public Collection<User> readByRealName(String realName) {
//        // Just a delegation of query
//        return repository.findAllByRealName(realName);
//    }

    @Override
    public User create(User data) {
        if (repository.existsById(data.getId()))
            throw new EntityCannotBeCreatedException("This username already exists");
        return repository.save(data);
    }
}
