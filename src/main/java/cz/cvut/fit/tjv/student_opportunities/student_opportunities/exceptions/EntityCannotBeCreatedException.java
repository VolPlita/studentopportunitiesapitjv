package cz.cvut.fit.tjv.student_opportunities.student_opportunities.exceptions;

public class EntityCannotBeCreatedException extends RuntimeException {
    public EntityCannotBeCreatedException(String message) {
        super(message);
    }

    public EntityCannotBeCreatedException(String message, Throwable cause) {
        super(message, cause);
    }
}
